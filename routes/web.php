<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::resource('user', 'UserController');
Route::resource('pasien', 'PasienController');
Route::resource('obat', 'ObatController');
Route::resource('bpjs', 'BpjsController');
Route::resource('dokter', 'DokterController');
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pasien/export', 'PasienController@export');
Route::get('/obat/export', 'ObatController@export');
Route::get('/dokter/export', 'DokterController@export');
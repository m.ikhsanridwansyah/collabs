@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card">
        <div class="card-header">
          <h2 class="card-title">Data Pasien</h2><br>
            <div class="card-title">
              <a href="/pasien/export" class="btn btn-sm btn-primary">Download</a>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('Success'))
            <div class="alert alert-success">
                {{ session('Success') }}
            </div>
            @endif
        
          <table class="table table-bordered">
            <thead>                  
                <tr>
                    <th style="width: 10px">No.</th>
                    <th>No. BPJS</th>
                    <th>Nama Pasien</th>
                    <th>Alamat Pasien</th>
                    <th>Telepon</th>
                    <th>Jenis Kelamin</th>
                    
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
             @forelse ($pasien as $key => $pasien1)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $pasien1->no_bpjs}} </td>
                    <td> {{ $pasien1->pasien['nama_pasien'] }} </td>
                    <td> {{ $pasien1->pasien['almtPasien'] }} </td>
                    <td> {{ $pasien1->pasien['NmrHp_pasien'] }} </td>
                    <td> {{ $pasien1->pasien['jenis_kelamin'] }} </td>
                    
                    <td style="display: flex;">
                    <a href="{{ route ('pasien.show' , ['pasien' => $pasien1->id])}}" class="btn btn-info btn-sm ml-1 mr-1">Show</a>
                    <a href="{{ route ('pasien.edit' , ['pasien' => $pasien1->id])}}" class="btn btn-success btn-sm ml-1 mr-1">Edit</a>
                    <form action="{{ route ('pasien.destroy' , ['pasien' => $pasien1->id])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                </tr>
             @empty
                 <tr>
                     <td colspan="7" align="center">Tidak Ada Data</td>
                 </tr>
             @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> --}}
      </div>
</div>
@endsection
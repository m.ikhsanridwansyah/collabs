@extends('adminlte.master')

@section('content')
            <div class="ml-3 mt-3 mr-3">
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Pendaftaran Pasien</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/pasien" method="POST">
                        @csrf
                        <div class="card-body">
                        <div class="form-group">
                            <label for="BPJS ID">No. BPJS</label> <br>
                            <input type="text" class="form" id="no_bpjs" name="no_bpjs" value="{{ old('no_bpjs', '') }}" placeholder="Nomor BPJS">
                            @error('Nama BPJS')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>



                        
                            <div class="form-group">
                            <label for="Nama Pasien">Nama Pasien</label> <br>
                            <input type="text" class="form" id="nama_pasien" name="nama_pasien" value="{{ old('nama_pasien', '') }}" placeholder="Nama Pasien">
                            @error('Nama Pasienl')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="Alamat">Alamat</label> <br>
                            <!-- <input type="text" class="form-control" id="almtPasien" name="almtPasien"  value="{{ old('almtPasien', '') }}" placeholder="Isi"> -->
                            <textarea id="almtPasien" name="almtPasien" class="form" cols="50" rows="10" value="{{ old('almtPasien', '') }}" placeholder="Alamat"></textarea>
                            @error('Alamat')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="NmrHp_pasien">Nomor HP</label> <br>
                            <input type="text" class="form" id="NmrHp_pasien" name="NmrHp_pasien"  value="{{ old('NmrHp_pasien', '') }}" placeholder="Nomor Hp">
                            @error('NmrHp_pasien')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label> <br>
                            <select type="text" class="form" id="jenis_kelamin" name="jenis_kelamin"  value="{{ old('jenis_kelamin', '') }}">
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                            @error('jenis_kelamin')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>
                            
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>


@endsection
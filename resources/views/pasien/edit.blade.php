@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Update Pasien</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="{{ route ('pasien.update' , ['pasien' => $pasien->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card-body">

                            <div class="form-group">
                            <label for="Nama Pasien">No BPJS</label>
                            <input type="text" class="form-control" id="no_bpjs" name="no_bpjs" value="{{ old('no_bpjs', $pasien->no_bpjs) }}" placeholder="Enter Judul">
                            @error('No bpjs')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror

                            <div class="form-group">
                            <label for="Nama Pasien">Nama Pasien</label>
                            <input type="text" class="form-control" id="nama_pasien" name="nama_pasien" value="{{ old('nama_pasien', $pasien->nama_pasien) }}" placeholder="Enter Judul">
                            @error('Nama Pasien')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="Alamat">Alamat</label>
                            <input type="text" class="form-control" id="almtPasien" name="almtPasien"  value="{{ old('almtPasien',  $pasien->almtPasien) }}" placeholder="Isi">
                            @error('Alamat')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="NmrHp_pasien">Nomor HP</label>
                            <input type="text" class="form-control" id="NmrHp_pasien" name="NmrHp_pasien"  value="{{ old('NmrHp_pasien',  $pasien->NmrHp_pasien) }}" placeholder="Nomor Hp">
                            @error('NmrHp_pasien')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>

                            <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label><br>
                            <select type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin"  value="{{ old('jenis_kelamin', '') }}">
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                            @error('jenis_kelamin')
                            <div class="alert alert-danger">{{ $message }}</div>     
                            @enderror
                            </div>
                            
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{ url()->previous() }}" class="btn btn-warning"></i>Back</a>
                        </div>
                    </form>
                </div>
</div>



@endsection
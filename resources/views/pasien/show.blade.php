@extends('adminlte.master')

@section('content') 
    <div class="ml-3 mt-3 mr-3">
        <h2 for="data_diri">Data Diri</h2>
        <table id="example" class="table table-bordered table-striped">
            <tr>
                <th style="width: 300px">Id</th>
                <th>{{ $pasien->id }}</th>
            </tr>
            <tr>
                <th style="width: 300px">ID BPJS</th>
                <th>{{ $pasien->bpjs_id }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Nama Pasien </th>
                <th>{{ $pasien->nama_pasien }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Alamat Pasien </th>
                <th>{{ $pasien->almtPasien }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Nomor Hp </th>
                <th>{{ $pasien->NmrHp_pasien }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Jenis Kelamin </th>
                <th>{{ $pasien->jenis_kelamin }}</th>
            </tr>
        </table>
    </div>
@endsection
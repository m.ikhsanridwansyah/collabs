@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Update Dokter</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="{{ route ('dokter.update' , ['dokter' => $dokter->id])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                <label for="nik">NIK</label>
                <input type="text" class="form-control" id="nik" name="nik"  value="{{ old('nik',  $dokter->nik) }}" placeholder="nik">
                @error('nik')
                <div class="alert alert-danger">{{ $message }}</div>     
                @enderror
                </div>

                <div class="form-group">
                <label for="Nama Dokter">Nama Dokter</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $dokter->name) }}" placeholder="Enter Judul">
                @error('Nama Dokter')
                <div class="alert alert-danger">{{ $message }}</div>     
                @enderror
                </div>

                <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email"  value="{{ old('email',  $dokter->email) }}" placeholder="email">
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>     
                @enderror
                </div>

                <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat"  value="{{ old('alamat',  $dokter->alamat) }}" placeholder="alamat">
                @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>     
                @enderror
                </div>

                <div class="form-group">
                <label for="password">Password</label><br>
                <input type="text" class="form-control" id="password" name="password"  value="{{ old('password',  $dokter->password) }}" placeholder="pass">
                @error('password')
                <div class="alert alert-danger">{{ $message }}</div>     
                @enderror
                </div>
                
                
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ url()->previous() }}" class="btn btn-warning"></i>Back</a>
            </div>
        </form>
    </div>
</div>
@endsection
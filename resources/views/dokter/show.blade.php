@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3 mr-3">
        <h2 for="data_diri">Data Diri</h2>
        <table id="example" class="table table-bordered table-striped">
            <tr>
                <th style="width: 300px">Id</th>
                <th>{{ $dokter->id }}</th>
            </tr>
            <tr>
                <th style="width: 300px">NIK </th>
                <th>{{ $dokter->nik }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Nama Dokter </th>
                <th>{{ $dokter->name }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Email </th>
                <th>{{ $dokter->email }}</th>
            </tr>
            <tr>
                <th style="width: 300px">Alamat </th>
                <th>{{ $dokter->alamat }}</th>
            </tr>
          
        </table>
    </div>
@endsection
@extends('adminlte.master')

@section('content')
            <div class="ml-3 mt-3 mr-3">    
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Pendaftaran Dokter</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/dokter" method="POST">
                        @csrf
                        <div class="card-body">
                        <div class="form-group">
                                <label for="BPJS ID">NIK</label> <br>
                                <input type="text" class="form" id="nik" name="nik" value="{{ old('nik', '') }}" placeholder="NIK">
                                @error('Nama BPJS')
                                <div class="alert alert-danger">{{ $message }}</div>     
                                @enderror
                            </div>                      
                                <div class="form-group">
                                <label for="Nama Dokter">Nama Dokter</label> <br>
                                <input type="text" class="form" id="name" name="name" value="{{ old('name', '') }}" placeholder="Nama Dokter">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>     
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="Alamat">Alamat</label> <br>                          
                                <textarea id="alamat" name="alamat" class="form" cols="50" rows="10" value="{{ old('alamat', '') }}" placeholder="Alamat"></textarea>
                                @error('Alamat')
                                <div class="alert alert-danger">{{ $message }}</div>     
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="Email">email</label> <br>
                                <input type="text" class="form" id="email" name="email"  value="{{ old('email', '') }}" placeholder="Email">
                                @error('Email')
                                <div class="alert alert-danger">{{ $message }}</div>     
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">password</label> <br>
                                <input type="password" class="form" id="password" name="password" placeholder="Password">
                                @error('Email')
                                <div class="alert alert-danger">{{ $message }}</div>     
                                @enderror
                            </div>
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>




@endsection
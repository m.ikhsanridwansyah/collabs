@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Dokter/Admin</h3><br>
                    <a href="/obat/export" class="btn btn-sm btn-primary">Download</a>
                    </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @if (session('Success'))
                    <div class="alert alert-success">
                        {{ session('Success') }}
                    </div>
                    @endif
                
                <table class="table table-bordered">
                    <thead>                  
                        <tr>
                            <th style="width: 10px">No.</th>
                            <th>NIK</th>
                            <th>Nama Dokter</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($dokter as $key => $dokter1)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $dokter1->nik }} </td>
                            <td> {{ $dokter1->name }} </td>
                            <td> {{ $dokter1->email }} </td>
                            <td> {{ $dokter1->alamat }} </td>
                            <td style="display: flex;">
                            <a href="{{ route ('dokter.show' , ['dokter' => $dokter1->id])}}" class="btn btn-info btn-sm ml-1 mr-1">Show</a>
                            <a href="{{ route ('dokter.edit' , ['dokter' => $dokter1->id])}}" class="btn btn-success btn-sm ml-1 mr-1">Edit</a>
                            <form action="{{ route ('dokter.destroy' , ['dokter' => $dokter1->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center">Tidak Ada Data</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
                </div> --}}
            </div>
        </div>
@endsection
@extends('adminlte.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Obat</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/obat" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                        <label for="kode_obat">Kode Obat</label> <br>
                        <input type="text" class="form" id="kode_obat" name="kode_obat" value="{{ old('kode_obat', '') }}" placeholder="Enter Obat">
                        @error('kode_obatl')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="nama_obat">Nama Obat</label> <br>
                        <input type="text" class="form" id="nama_obat" name="nama_obat"  value="{{ old('nama_obat', '') }}" placeholder="Nama Obat">
                        @error('nama_obat')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="satuan">Satuan</label> <br>
                        <input type="number" class="form" id="satuan" name="satuan"  value="{{ old('satuan', '') }}" placeholder="Satuan">
                        @error('satuan')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="harga_satuan">Harga Satuan</label> <br>
                        <input type="text" class="form" id="harga_obat" name="harga_obat"  value="{{ old('harga_obat', '') }}" placeholder="Isi">
                        @error('harga_satuan')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>
                        
                        
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>

@endsection 
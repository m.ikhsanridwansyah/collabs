@extends('adminlte.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Update Obat</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ route ('obat.update' , ['obat' => $obat->kode_obat])}}" method="POST">
                    @csrf
                    @method('PUT') 
                    <div class="card-body">
                        <div class="form-group">
                        <label for="kode_obat">Kode Obat</label> <br>
                        <input type="text" class="form" id="kode_obat" name="kode_obat" value="{{ old('kode_obat', $obat->kode_obat) }}" placeholder="Enter Obat">
                        @error('kode_obat')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="nama_obat">Nama Obat</label> <br>
                        <input type="text" class="form" id="nama_obat" name="nama_obat"  value="{{ old('nama_obat', $obat->nama_obat) }}" placeholder="Nama Obat">
                        @error('nama_obat')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="satuan">Satuan</label> <br>
                        <input type="number" class="form" id="satuan" name="satuan"  value="{{ old('satuan', $obat->satuan) }}" placeholder="Satuan">
                        @error('satuan')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>

                        <div class="form-group">
                        <label for="harga_satuan">Harga Satuan</label> <br>
                        <input type="text" class="form" id="harga_obat" name="harga_obat"  value="{{ old('harga_obat', $obat->harga_obat) }}" placeholder="Isi">
                        @error('harga_satuan')
                        <div class="alert alert-danger">{{ $message }}</div>     
                        @enderror
                        </div>
                        
                        
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="{{ url()->previous() }}" class="btn btn-warning"></i>Back</a>
                    </div>
                </form>
            </div>
        </div>



@endsection
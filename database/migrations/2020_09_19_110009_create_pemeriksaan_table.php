<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemeriksaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan', function (Blueprint $table) {
            $table->unsignedBigInteger('pasien_id');
            $table->foreign('pasien_id')->references('id')->on('pasien');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('obat_kode_obat');
            $table->foreign('obat_kode_obat')->references('kode_obat')->on('obat');
            $table->string('hasil_pemeriksaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('pemeriksaan');
        //     $table->dropColumn(['hasil_pemeriksaan']);
        //     $table->dropForeign(['obat_kode_obat']);
        //     $table->dropColumn(['obat_kode_obat']);
        //     $table->dropForeign(['user_id']);
        //     $table->dropColumn(['user_id']);
        //     $table->dropForeign(['pasien_id']);
        //     $table->dropColumn(['pasien_id']);
    }
}

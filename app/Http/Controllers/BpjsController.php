<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Bpjs;
class BpjsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bpjs = Bpjs::all();
      
        return view('bpjs.index' , compact('bpjs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bpjs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_bpjs' => 'required|unique:bpjs|max:255',
            'nama_pemegang_kartu ' => 'required',
            
            ]);


            $Bpjs = Bpjs::create([
                "no_bpjs" => $request["no_bpjs"],
                "nama_pemegang_kartu"  => $request["nama_pemegang_kartu"],
                
            ]);
    
                return redirect('/bpjs')->with('Success', 'Postingan berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bpjs = Bpjs::find($id);
        return view('bpjs.edit', compact('bpjs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Bpjs::where('id' , $id)->update([
            "no_bpjs" => $request["no_bpjs"],
            "nama_pemegang_kartu" => $request["nama_pemegang_kartu"],
    ]);

    return redirect('/bpjs')->with('Success', 'Berhasil update pertanyaan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bpjs::destroy($id);
        return redirect('/bpjs')->with('Success', 'Berhasil delete pertanyaan!');
    }
}

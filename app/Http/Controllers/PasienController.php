<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pasien;
use App\Bpjs;
use App\Exports\PasienExport;
use Maatwebsite\Excel\Facades\Excel;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $pasien = Bpjs::all();
      
        return view('pasien.index' , compact('pasien'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
        
    public function store(Request $request) {
    //    dd($request->all());
        $request->validate([
            'nama_pasien' => 'required|unique:pasien|max:255',
            'almtPasien' => 'required',
            'NmrHp_pasien' => 'required',
            
            ]);

            

            // Pasien::create($request->all());  bisa make yang ini 
            $data = new Bpjs;

            $data->nama_pemegang_kartu = $request["nama_pasien"];
            $data->no_bpjs = $request["no_bpjs"];

            if ($data->save()) {
                $data->pasien()->save(new Pasien([
                    "nama_pasien" => $request["nama_pasien"],
                    "almtPasien"  => $request["almtPasien"],
                    "NmrHp_pasien" => $request["NmrHp_pasien"],
                    'jenis_kelamin' => $request['jenis_kelamin']
    
                ]));
            }
    
            return redirect('/pasien')->with('Success', 'Postingan berhasil disimpan!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pasien = Pasien::find($id);
        return view('pasien.show', compact('pasien'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pasien = Pasien::find($id);
        return view('pasien.edit', compact('pasien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Bpjs::where('id' , $id)->update([
            "no_bpjs" => $request ['no_bpjs'],
        ]);
        $update = Pasien::where('id' , $id)->update([
                
                "nama_pasien" => $request["nama_pasien"],
                "almtPasien"  => $request["almtPasien"],
                "NmrHp_pasien" => $request["NmrHp_pasien"],
                'jenis_kelamin' => $request['jenis_kelamin']
        ]);

        return redirect('/pasien')->with('Success', 'Berhasil update pertanyaan!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bpjs::where('id', $id)->delete();
        return redirect('/pasien')->with('Success', 'Berhasil delete pertanyaan!');
    }

    public function export() 
    {
        return Excel::download(new PasienExport, 'pasien.xlsx');
    }
}

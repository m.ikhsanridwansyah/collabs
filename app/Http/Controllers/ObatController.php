<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Obat;
use App\Exports\ObatExport;
use Maatwebsite\Excel\Facades\Excel;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obat = Obat::all();
      
        return view('obat.index' , compact('obat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('obat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //    dd($request->all());
        $request->validate([
            'kode_obat' => 'required|unique:Obat|max:255',
            'nama_obat' => 'required',
            'satuan' => 'required',
            'harga_obat' => 'required',
            
            ]);

            // Obat::create($request->all());  bisa make yang ini 
            
            $Obat = Obat::create([
                "kode_obat" => $request["kode_obat"],
                "nama_obat"  => $request["nama_obat"],
                "satuan" => $request["satuan"],
                'harga_obat' => $request['harga_obat']
            ]);
    
                return redirect('/obat')->with('Success', 'Postingan berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kode_obat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_obat)
    {
        // $obat = Obat::find($kode_obat);
        $obat = Obat::findOrFail($kode_obat);
        return view('obat.edit', compact('obat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_obat)
    {
        $update = Obat::where('kode_obat' , $kode_obat)->update([
                "nama_obat"  => $request["nama_obat"],
                "satuan" => $request["satuan"],
                'harga_obat' => $request['harga_obat']
    ]);

    return redirect('/obat')->with('Success', 'Berhasil update pertanyaan!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_obat)
    {
        Obat::destroy($kode_obat);
        return redirect('/obat')->with('Success', 'Berhasil delete pertanyaan!');
    }

    public function export() 
    {
        return Excel::download(new ObatExport, 'obat.xlsx');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';  // mengammbil dari table pasien
    protected $guarded =[];
    
    public function bpjs(){
        return $this->belongsTo('App\Bpjs' , 'bpjs_id'); // memanggil user_id dari table user untuk profil
    }
}

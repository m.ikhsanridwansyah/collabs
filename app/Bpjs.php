<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bpjs extends Model
{
    protected $table = 'bpjs';  // mengammbil dari table pasien
    protected $guarded =[]; 


    public function pasien(){
        return $this->hasOne('App\Pasien', 'bpjs_id');
    }
}
    
